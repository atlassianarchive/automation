## Atlassian Automation plugin
This page is a placeholder for Atlassian Automation plugin, which has been moved to [https://bitbucket.org/atlassianlabs/automation](https://bitbucket.org/atlassianlabs/automation). Please update your bookmarks/relocate your `git` repository.
